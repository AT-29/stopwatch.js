export default () => {
  let startTime;
  let endTime;
  let direction;
  let timeGap;
  let timeInSeconds;
  let tickSettings = {startTime:0,endTime:0,speed:1};
  const addTickEvent = (settings,callback)=>{
     startTime = settings.startTime || tickSettings.startTime;
     endTime = settings.endTime || tickSettings.endTime;
     timeGap = settings.speed || tickSettings.speed;
     tickSettings = settings;
     timeInSeconds = 0 + startTime || 0;
     direction  = startTime <= endTime ? 1 : -1;
    if(typeof callback === 'function'){
      tickEvent = ()=>callback(timeInSeconds);
    }
  }

  let timeStamps = [];
  let cacheTimeStamps = [];
  let pause = false;
  let timeout;
  let tickEvent;
  let eventMap = new Map();

  const countTime = () =>{
      timeout = setTimeout(()=>{
      tickEvent();
      if(!pause &&  (direction === 1 && timeInSeconds<=endTime) ||  (direction === -1 && timeInSeconds>=endTime)){
        const currentEvent = eventMap.get(timeInSeconds);
          if(typeof currentEvent === 'function'){
            currentEvent();
          }
        countTime();
        timeInSeconds+=1*direction;
      }
    },1000*timeGap);

  }

  const reachTime = () =>{
    timeout = setTimeout(()=>{
      if(!pause ){
        const current = timeStamps.pop();
        const currentEvent = eventMap.get(current);
          if(typeof currentEvent === 'function'){
            currentEvent();
          }
          reachTime();
      }
    },timeStamps[timeStamps.length-1]*1000);
  }

  const addTimeEvents = (...events) =>{
    eventMap = events.reduce((map, event) => {
      if(!Number.isInteger(event[0])){
        throw new Error('Event Time should be a valid integer - but instead got:' + event[0])
      }
      map.set(event[0], event[1]);
      return map;
    }, eventMap);

    timeStamps = [...eventMap.keys()].sort((a,b)=>b-a);
    cacheTimeStamps = [...timeStamps];
  
  }
  const start = () =>{
    pause = false;
   if(!timeout){
    if(!tickEvent){
      reachTime();
    }else{
      countTime()
    }
   }
  };
  const stop = () =>{
    pause = true;
    clearTimeout(timeout);
  };
  const reset = () =>{  
    pause = true;
    startTime = tickSettings.startTime || 0;
    timeInSeconds = 0 + startTime;
    timeStamps = [...cacheTimeStamps];
    clearTimeout(timeout);
  };
  return {start,stop,reset,addTimeEvents,addTickEvent}
};


