# StopWatch.js

Simple time based event library
```javascript
<h1 id="timerElement">0</h1>
<script type="module" >
import timer from "./timer.js"
const timerElement = document.getElementById('timerElement');
const myTimer = timer();
myTimer.addTickEvent({startTime:0,endTime:20,speed:1},(currentTime)=>timerElement.innerHTML = currentTime); //optional
myTimer.addTimeEvents([5,()=>timerElement.style.color = 'red'],[10,()=>timerElement.style.color = 'black'],[20,()=>{myTimer.reset();myTimer.start()}])
myTimer.start();
</script>
```
